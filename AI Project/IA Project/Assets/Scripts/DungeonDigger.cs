﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DungeonDigger : MonoBehaviour {

    [Range(10, 100)]
    public int dungeonSize = 100;
    [Range(1, 100)]
    public int roomNumber = 10;
    [Range(1, 10)]
    public int minHallwayTiles = 2;
    [Range(1, 10)]
    public int maxHallwayTiles = 4;
    [Range(2, 10)]
    public int minRoomLength = 2;
    [Range(2, 10)]
    public int maxRoomLength = 4;
    [Range(2, 10)]
    public int minRoomWidth = 2;
    [Range(2, 10)]
    public int maxRoomWidth = 4;

    public GameObject tile;
    public Material roomMaterial;
    public Material hallwayMaterial;

    public GameObject playerController;

    private GameObject dungeon;
    private float xSize;
    private float zSize;
    private Vector3 diggerPos;
    private float stepLenght;

    //le posizioni già occupate
    private List<Vector3> usedPositions = new List<Vector3>();

    //i tile che compongono sia le stanze che i corridoi
    private List<List<GameObject>> rooms = new List<List<GameObject>>();
    private List<List<GameObject>> hallways = new List<List<GameObject>>();

    //posizioni da cui posso ripartira a scavare, con specificata l'appartenenza a stanza o corriodio (true = stanza, false = corridoio)
    private List<KeyValuePair<Vector3, bool>> exploitablePos = new List<KeyValuePair<Vector3, bool>>();
    //tile dei muri appartenenti ad una certa posizione e con specificata la direzione in cui sono rivolti
    private Dictionary<Vector3, Dictionary<Vector2, GameObject>> wallTilePos = new Dictionary<Vector3, Dictionary<Vector2, GameObject>>(); 

	// Use this for initialization
	void Start () {

        stepLenght = tile.GetComponent<MeshFilter>().sharedMesh.bounds.size.x * tile.transform.localScale.x;

        //parent per corridoi e stanze
        GameObject roomParent = new GameObject("Rooms");
        GameObject hallwayParent = new GameObject("Hallways");

        //grandezza del dungeon
        dungeon = GameObject.CreatePrimitive(PrimitiveType.Plane);
        dungeon.name = "Dungeon Area";
        dungeon.transform.position = new Vector3(0, -0.1f, 0);
        dungeon.transform.localScale = new Vector3(dungeonSize, 1, dungeonSize);

        //posizione di partenza del digger
        Mesh mesh = dungeon.GetComponent<MeshFilter>().mesh;
        Bounds bounds = mesh.bounds;
        xSize = bounds.size.x * dungeon.transform.localScale.x;
        zSize = bounds.size.z * dungeon.transform.localScale.z;
        float randomX = Random.Range(dungeon.transform.position.x - (xSize / 2 - stepLenght), dungeon.transform.position.x + (xSize / 2 - stepLenght));
        float randomZ = Random.Range(dungeon.transform.position.z - (zSize / 2 - stepLenght), dungeon.transform.position.z + (zSize / 2 - stepLenght));
        gameObject.transform.position = new Vector3(randomX, 0, randomZ);
        diggerPos = gameObject.transform.position;

        //inzio creazione corridoi e stanze

        int numRooms = 0;
        int roomProb = 5;
        int hallwayProb = 5;
        GameObject tmpTile;
        Vector2 direction = Vector2.one;
        Vector3 tmpPos = Vector3.one;
        bool done = false;
        int stop = 0;
        int maxAttempts = roomNumber * 10;

        //barra progresso
        float progressBar = 0f;
        EditorUtility.DisplayProgressBar("Dungeon creation.", "Placing tiles...", progressBar);

        //finche non si raggiunge numero di stanze volute(o grandezza)
        while (numRooms < roomNumber)
        {
            //creazione corridoio basata su una certa probabilità
            if (Random.Range(0,100) <= hallwayProb)
            {
                done = false;
                stop = 0;

                tile.GetComponent<Renderer>().sharedMaterial = hallwayMaterial;
                foreach(Transform child in tile.transform)
                {
                    child.GetComponent<Renderer>().sharedMaterial = hallwayMaterial;
                }

                Vector3 savedPos = diggerPos;

                //organizzazione dei tile del corridoio creati con il giusto parent
                List<GameObject> hallwayTiles = new List<GameObject>();
                GameObject hallway = new GameObject();
                hallway.transform.parent = hallwayParent.transform;

                while (!done) //si esce dal ciclo solo una volta costruito tutto il corridoio o quando si è bloccato per almeno tot volte
                {
                    int check = 0;
                    Vector3 rndPos = Vector3.one;

                    if (stop > maxAttempts / 10 && stop < maxAttempts / 4) //se faccio fatica a costruire nuovi corridoi, provo a creare una nuova via usando un tile random tra tutti quelli validi delle stanze
                    {
                        rndPos = GetRandomValidPos(2);
                        if (rndPos != Vector3.one)
                        {
                            diggerPos = rndPos;
                        }
                    } else if(stop > maxAttempts / 4)
                    {
                        //se faccio ancora fatica, prendo anche i tile validi tra i corridoi già creati
                        rndPos = GetRandomValidPos(0);
                        if (rndPos != Vector3.one)
                        {
                            diggerPos = rndPos;
                        }
                    }

                    Vector3 markPos = diggerPos;

                    //direzione random dal tile di partenza ma se faccio molta fatica suggerisco una direzione e un tile random valido in cui ne sono presenti meno
                    if(stop < maxAttempts / 2)
                    {
                        direction = GetRandomDir();
                    }
                    else
                    {
                        KeyValuePair<Vector2, Vector3> dirAndPos = GetOptimalDirAndOptimalRndPos();
                        if (dirAndPos.Key != Vector2.zero)
                        {
                            direction = dirAndPos.Key;
                        }
                        if (dirAndPos.Value != Vector3.one)
                        {
                            rndPos = dirAndPos.Value;
                            diggerPos = rndPos;
                            markPos = diggerPos;
                        }
                    }
                    

                    //lunghezza scelta a random del nuovo corridoio
                    int tmpDst = Random.Range(minHallwayTiles, maxHallwayTiles + 1);

                    for (int i = 0; i < tmpDst; i++)
                    {
                        tmpPos = diggerPos + new Vector3(direction.x, 0, direction.y) * stepLenght;

                        //si scava solo se rimane nei limiti del dungeon
                        if (IsInDungeon(tmpPos))
                        {
                            //controllo che non ci siano altri tile nella posizione in cui dovrei piazzare quello nuovo
                            if (IsPosFree(tmpPos))
                            {
                                diggerPos = tmpPos;
                                gameObject.transform.position = diggerPos;
                                tmpTile = Instantiate(tile, diggerPos, Quaternion.identity, hallway.transform);
                                tmpTile.name = "Floor";
                                usedPositions.Add(tmpTile.transform.position);
                                if(i == 0 || i == tmpDst - 1)
                                {
                                    hallwayTiles.Add(tmpTile);
                                }
                                else
                                {
                                    hallwayTiles.Add(tmpTile);
                                    exploitablePos.Add(new KeyValuePair<Vector3, bool>(tmpTile.transform.position, false));
                                }

                                //aggiungo i muri

                                Vector3 lastTilePos = tmpTile.transform.position;
                                wallTilePos.Add(lastTilePos, new Dictionary<Vector2, GameObject>());

                                //capo e coda del corriodio
                                if (i == 0 && usedPositions.Count == 1)
                                {
                                    tmpPos = diggerPos + new Vector3(-direction.x, 1, -direction.y) * (stepLenght / 2);
                                    tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, hallway.transform);
                                    if (direction == Vector2.up || direction == Vector2.down)
                                    {
                                        tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                    }
                                    else
                                    {
                                        tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                    }
                                    hallwayTiles.Add(tmpTile);

                                    wallTilePos[lastTilePos].Add(-direction, tmpTile);
                                }
                                else if (i == tmpDst - 1)
                                {
                                    tmpPos = diggerPos + new Vector3(direction.x, 1, direction.y) * (stepLenght / 2);
                                    tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, hallway.transform);
                                    if (direction == Vector2.up || direction == Vector2.down)
                                    {
                                        tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                    }
                                    else
                                    {
                                        tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                    }
                                    hallwayTiles.Add(tmpTile);

                                    wallTilePos[lastTilePos].Add(direction, tmpTile);
                                }


                                //muri laterali
                                if (direction == Vector2.up || direction == Vector2.down)
                                {
                                    tmpPos = diggerPos + new Vector3(Vector2.left.x, 1, Vector2.left.y) * (stepLenght / 2);
                                    tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, hallway.transform);
                                    tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                    hallwayTiles.Add(tmpTile);

                                    wallTilePos[lastTilePos].Add(Vector2.left, tmpTile);

                                    tmpPos = diggerPos + new Vector3(Vector2.right.x, 1, Vector2.right.y) * (stepLenght / 2);
                                    tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, hallway.transform);
                                    tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                    hallwayTiles.Add(tmpTile);

                                    wallTilePos[lastTilePos].Add(Vector2.right, tmpTile);
                                }
                                else
                                {
                                    tmpPos = diggerPos + new Vector3(Vector2.up.x, 1, Vector2.up.y) * (stepLenght / 2);
                                    tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, hallway.transform);
                                    tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                    hallwayTiles.Add(tmpTile);

                                    wallTilePos[lastTilePos].Add(Vector2.up, tmpTile);

                                    tmpPos = diggerPos + new Vector3(Vector2.down.x, 1, Vector2.down.y) * (stepLenght / 2);
                                    tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, hallway.transform);
                                    tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                    hallwayTiles.Add(tmpTile);

                                    wallTilePos[lastTilePos].Add(Vector2.down, tmpTile);
                                }

                                check++;
                            }
                        }
                    }

                    if (check == tmpDst) //se sono riuscito a piazzare tutti i tile vado avanti
                    {
                        done = true;
                        hallways.Add(hallwayTiles);
                        hallwayProb = 0;
                        if (rndPos != Vector3.one)
                        {
                            exploitablePos.Remove(exploitablePos.Find(item => item.Key.Equals(rndPos)));

                            //elimino l'eventuale muro dell'altro corridoio/stanza che blocca il passaggio
                            DestroyImmediate(wallTilePos[rndPos][direction]);
                        } else if(hallways.Count > 1 || rooms.Count >= 1)
                        {
                            DestroyImmediate(wallTilePos[markPos][direction]);
                        }
                    }
                    else //altrimenti elimino quelli eventualmente fatti e riprovo
                    {
                        foreach (GameObject t in hallwayTiles)
                        {
                            exploitablePos.Remove(exploitablePos.Find(item => item.Key.Equals(t.transform.position)));
                            usedPositions.Remove(t.transform.position);

                            wallTilePos.Remove(t.transform.position);

                            DestroyImmediate(t);
                        }
                        hallwayTiles.Clear();
                        diggerPos = savedPos;
                    }

                    //aggiorno la barra del progresso
                    progressBar = ((float)(check * 100) / tmpDst);
                    EditorUtility.DisplayProgressBar("Corridor creation.", "Placing corridor tiles...", progressBar);

                    stop++;

                    if (stop > maxAttempts) //se non si trova più spazio per via della grandezza del dungeon o della disposizione di corridoi e stanze si termina
                    {
                        numRooms = roomNumber;
                        done = true;
                    }
                }

                if(hallway.transform.childCount == 0) //se non sono riuscito a creare il corridoio elimino il genitore
                {
                    DestroyImmediate(hallway);
                }
            }
            else //incremento la probabilità di fare corridoio in caso non lo abbia fatto
            {
                hallwayProb += 5;
            }

            //creazione stanza
            if (Random.Range(0,100) <= roomProb)
            {
                done = false;
                stop = 0;

                tile.GetComponent<Renderer>().sharedMaterial = roomMaterial;
                foreach (Transform child in tile.transform)
                {
                    child.GetComponent<Renderer>().sharedMaterial = roomMaterial;
                }

                Vector3 savedPos = diggerPos;

                //organizzazione stanze con genitore
                List<GameObject> roomTiles = new List<GameObject>();
                GameObject room = new GameObject();
                room.transform.parent = roomParent.transform;

                while (!done)
                {
                    int check = 0;
                    Vector3 rndPos = Vector3.one;
                    if (stop > maxAttempts / 10 && stop < maxAttempts / 4) //se faccio fatica a costruire nuove stanze, provo a creare una nuova via usando un tile random tra tutti quelli validi dei corridoi
                    {
                        rndPos = GetRandomValidPos(1);
                        if (rndPos != Vector3.one)
                        {
                            diggerPos = rndPos;
                        }
                    } else if(stop > maxAttempts / 4)
                    {
                        //se faccio ancora fatica prendo un tile valido anche tra le stanze già create
                        rndPos = GetRandomValidPos(0);
                        if (rndPos != Vector3.one)
                        {
                            diggerPos = rndPos;
                        }
                    }

                    Vector3 markPos = diggerPos;

                    //direzione random dal tile di partenza ma se faccio fatica suggerisco una direzione e una posizione random valida in cui sono presenti meno tile
                    if (stop < maxAttempts / 2)
                    {
                        direction = GetRandomDir();
                    }
                    else
                    {
                        KeyValuePair<Vector2, Vector3> dirAndPos = GetOptimalDirAndOptimalRndPos();
                        if(dirAndPos.Key != Vector2.zero)
                        {
                            direction = dirAndPos.Key;
                        }
                        if(dirAndPos.Value != Vector3.one)
                        {
                            rndPos = dirAndPos.Value;
                            diggerPos = rndPos;
                            markPos = diggerPos;
                        }
                    }

                    //grandezza random della stanza
                    int tmpLenght = Random.Range(minRoomLength, maxRoomLength + 1);
                    int tmpWidth = Random.Range(minRoomWidth, maxRoomWidth + 1);

                    for (int i = 0; i < tmpLenght; i++)
                    {
                        tmpPos = diggerPos + new Vector3(direction.x, 0, direction.y) * stepLenght;

                        for (int j = 0; j < tmpWidth; j++)
                        {
                            //si scava solo se rimane nei limiti del dungeon
                            if (IsInDungeon(tmpPos))
                            {
                                //controllo che non ci siano altri tile dove dovrei piazzare quello nuovo
                                if (IsPosFree(tmpPos))
                                {
                                    diggerPos = tmpPos;
                                    gameObject.transform.position = diggerPos;
                                    tmpTile = Instantiate(tile, diggerPos, Quaternion.identity, room.transform);
                                    tmpTile.name = "Floor";
                                    usedPositions.Add(tmpTile.transform.position);

                                    //distinguo i tile del bordo di una stanza da quelli interni
                                    if ((i == 0 && j != 0) || i == tmpLenght - 1)
                                    {
                                        roomTiles.Add(tmpTile);
                                        exploitablePos.Add(new KeyValuePair<Vector3, bool>(tmpTile.transform.position, true));
                                    } else if ((j == 0 && i != 0) || j == tmpWidth - 1)
                                    {
                                        roomTiles.Add(tmpTile);
                                        exploitablePos.Add(new KeyValuePair<Vector3, bool>(tmpTile.transform.position, true));
                                    }
                                    else
                                    {
                                        roomTiles.Add(tmpTile);
                                    }

                                    //aggiungo i muri

                                    Vector3 lastTilePos = tmpTile.transform.position;

                                    wallTilePos.Add(lastTilePos, new Dictionary<Vector2, GameObject>());

                                    //lato inziale e finale
                                    if(i == 0)
                                    {
                                        if(j != 0 || (j == 0 && usedPositions.Count == 1))
                                        {
                                            tmpPos = diggerPos + new Vector3(-direction.x, 1, -direction.y) * (stepLenght / 2);
                                            tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                            if (direction == Vector2.up || direction == Vector2.down)
                                            {
                                                tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                            }
                                            else
                                            {
                                                tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                            }
                                            roomTiles.Add(tmpTile);

                                            wallTilePos[lastTilePos].Add(-direction, tmpTile);
                                        }
                                    } else if(i == tmpLenght - 1)
                                    {
                                        tmpPos = diggerPos + new Vector3(direction.x, 1, direction.y) * (stepLenght / 2);
                                        tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                        if (direction == Vector2.up || direction == Vector2.down)
                                        {
                                            tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                        }
                                        else
                                        {
                                            tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                        }
                                        roomTiles.Add(tmpTile);

                                        wallTilePos[lastTilePos].Add(direction, tmpTile);
                                    }


                                    //muri laterali
                                    if (j == 0)
                                    {
                                        if (direction == Vector2.up || direction == Vector2.down)
                                        {
                                            if (i % 2 == 0)
                                            {
                                                tmpPos = diggerPos + new Vector3(Vector2.left.x, 1, Vector2.left.y) * (stepLenght / 2);
                                                tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                                tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                                roomTiles.Add(tmpTile);

                                                wallTilePos[lastTilePos].Add(Vector2.left, tmpTile);
                                            }
                                            else
                                            {
                                                tmpPos = diggerPos + new Vector3(Vector2.right.x, 1, Vector2.right.y) * (stepLenght / 2);
                                                tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                                tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                                roomTiles.Add(tmpTile);

                                                wallTilePos[lastTilePos].Add(Vector2.right, tmpTile);
                                            }
                                        }
                                        else
                                        {
                                            if (i % 2 == 0)
                                            {
                                                tmpPos = diggerPos + new Vector3(Vector2.down.x, 1, Vector2.down.y) * (stepLenght / 2);
                                                tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                                tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                                roomTiles.Add(tmpTile);

                                                wallTilePos[lastTilePos].Add(Vector2.down, tmpTile);
                                            }
                                            else
                                            {
                                                tmpPos = diggerPos + new Vector3(Vector2.up.x, 1, Vector2.up.y) * (stepLenght / 2);
                                                tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                                tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                                roomTiles.Add(tmpTile);

                                                wallTilePos[lastTilePos].Add(Vector2.up, tmpTile);
                                            }

                                        }
                                    }
                                    else if (j == tmpWidth - 1)
                                    {
                                        if (direction == Vector2.up || direction == Vector2.down)
                                        {
                                            if (i % 2 == 0)
                                            {
                                                tmpPos = diggerPos + new Vector3(Vector2.right.x, 1, Vector2.right.y) * (stepLenght / 2);
                                                tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                                tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                                roomTiles.Add(tmpTile);

                                                wallTilePos[lastTilePos].Add(Vector2.right, tmpTile);
                                            }
                                            else
                                            {
                                                tmpPos = diggerPos + new Vector3(Vector2.left.x, 1, Vector2.left.y) * (stepLenght / 2);
                                                tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                                tmpTile.transform.rotation = Quaternion.Euler(0, 0, 90);
                                                roomTiles.Add(tmpTile);

                                                wallTilePos[lastTilePos].Add(Vector2.left, tmpTile);
                                            }
                                            
                                        }
                                        else
                                        {
                                            if (i % 2 == 0)
                                            {
                                                tmpPos = diggerPos + new Vector3(Vector2.up.x, 1, Vector2.up.y) * (stepLenght / 2);
                                                tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                                tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                                roomTiles.Add(tmpTile);

                                                wallTilePos[lastTilePos].Add(Vector2.up, tmpTile);
                                            }
                                            else
                                            {
                                                tmpPos = diggerPos + new Vector3(Vector2.down.x, 1, Vector2.down.y) * (stepLenght / 2);
                                                tmpTile = Instantiate(tile, tmpPos, Quaternion.identity, room.transform);
                                                tmpTile.transform.rotation = Quaternion.Euler(90, 0, 0);
                                                roomTiles.Add(tmpTile);

                                                wallTilePos[lastTilePos].Add(Vector2.down, tmpTile);
                                            }
                                            
                                        }
                                    }


                                    check++;
                                }
                            }

                            //capire quale direzione si è presa prima e formare di conseguenza la stanza
                            Vector2 tmpDirection = Vector2.one;
                            if (i % 2 == 0)
                            {
                                if (direction == Vector2.up || direction == Vector2.down)
                                {
                                    tmpDirection = Vector2.right;
                                }
                                else
                                {
                                    tmpDirection = Vector2.up;
                                }
                            }
                            else
                            {
                                if (direction == Vector2.up || direction == Vector2.down)
                                {
                                    tmpDirection = Vector2.left;
                                }
                                else
                                {
                                    tmpDirection = Vector2.down;
                                }
                            }

                            tmpPos = diggerPos + new Vector3(tmpDirection.x, 0, tmpDirection.y) * stepLenght;
                        }
                    }
                    if (check == tmpLenght * tmpWidth) //controllo se sono stati creati tutti i tile
                    {
                        done = true;
                        roomProb = 0;
                        rooms.Add(roomTiles);
                        if (rndPos != Vector3.one)
                        {
                            exploitablePos.Remove(exploitablePos.Find(item => item.Key.Equals(rndPos)));

                            //elimino l'eventuale muro dell'altro corridoio/stanza che blocca il passaggio
                            DestroyImmediate(wallTilePos[rndPos][direction]);
                        }
                        else if (rooms.Count > 1 || hallways.Count >= 1)
                        {
                            DestroyImmediate(wallTilePos[markPos][direction]);
                        }
                        numRooms++;
                    }
                    else //sennò si eliminano le possibili create
                    {
                        foreach (GameObject t in roomTiles)
                        {
                            exploitablePos.Remove(exploitablePos.Find(item => item.Key.Equals(t.transform.position)));
                            usedPositions.Remove(t.transform.position);

                            wallTilePos.Remove(t.transform.position);

                            DestroyImmediate(t);
                        }
                        roomTiles.Clear();
                        diggerPos = savedPos;
                    }

                    //aggiorno la barra del progresso
                    progressBar = ((float)(check * 100) / (tmpLenght * tmpWidth));
                    EditorUtility.DisplayProgressBar("Room creation.", "Placing room tiles...", progressBar);

                    stop++;

                    if (stop > maxAttempts) //se non si trova più spazio per via della grandezza del dungeon o della dispozione di corridoi e stanze si termina
                    {
                        numRooms = roomNumber;
                        done = true;
                    }
                }

                if (room.transform.childCount == 0)
                {
                    DestroyImmediate(room);
                }
            }
            else
            {
                roomProb += 5;
            }
        }

        Debug.Log("Num stanze: " + rooms.Count);


        //aggiunta del giocatore
        GameObject instantiatedPlayer = Instantiate(playerController, diggerPos + new Vector3(0, stepLenght / 2, 0), Quaternion.identity);
        instantiatedPlayer.transform.localScale *= tile.transform.localScale.x;

        //finito il lavoro elimino la barra del progresso
        EditorUtility.ClearProgressBar();
    }

    //controllo se sono nei limiti del dungeon
    private bool IsInDungeon(Vector3 pos)
    {
        return (pos.x >= dungeon.transform.position.x - ((xSize / 2) - (stepLenght / 2))
                                && pos.x <= dungeon.transform.position.x + ((xSize / 2) - (stepLenght / 2))
                                && pos.z >= dungeon.transform.position.z - ((zSize / 2) - (stepLenght / 2))
                                && pos.z <= dungeon.transform.position.z + ((zSize / 2) - (stepLenght / 2)));
    }

    //controllo quale direzione del dungeon è più libera dai tile (considerando gli assi cardinali) e la ritorno insieme ad una posizione candidata in quella zona
    private KeyValuePair<Vector2, Vector3> GetOptimalDirAndOptimalRndPos()
    {
        List<Vector3> eastPos = new List<Vector3>();
        List<Vector3> westPos = new List<Vector3>();
        List<Vector3> southPos = new List<Vector3>();
        List<Vector3> northPos = new List<Vector3>();

        KeyValuePair<Vector2, Vector3> result = new KeyValuePair<Vector2, Vector3>(Vector2.zero, Vector3.one);

        foreach (Vector3 pos in usedPositions)
        {
            if(pos.x > dungeon.transform.position.x
                                && pos.z >= dungeon.transform.position.z - ((zSize / 2) - (stepLenght / 2))
                                && pos.z <= dungeon.transform.position.z + ((zSize / 2) - (stepLenght / 2)))
            {
                eastPos.Add(pos);
            } else if(pos.x < dungeon.transform.position.x
                                && pos.z >= dungeon.transform.position.z - ((zSize / 2) - (stepLenght / 2))
                                && pos.z <= dungeon.transform.position.z + ((zSize / 2) - (stepLenght / 2)))
            {
                westPos.Add(pos);
            } else if(pos.x >= dungeon.transform.position.x - ((xSize / 2) - (stepLenght / 2))
                                && pos.x <= dungeon.transform.position.x + ((xSize / 2) - (stepLenght / 2))
                                && pos.z < dungeon.transform.position.z)
            {
                southPos.Add(pos);
            } else if(pos.x >= dungeon.transform.position.x - ((xSize / 2) - (stepLenght / 2))
                                && pos.x <= dungeon.transform.position.x + ((xSize / 2) - (stepLenght / 2))
                                && pos.z > dungeon.transform.position.z)
            {
                northPos.Add(pos);
            }
        }

        Vector3 tmpPos = Vector3.one;
        bool found = false;

        if (eastPos.Count <= westPos.Count)
        {
            if (eastPos.Count <= southPos.Count)
            {
                if(eastPos.Count <= northPos.Count)
                {
                    if(eastPos.Count != 0)
                    {
                        while (!found)
                        {
                            tmpPos = eastPos[Random.Range(0, eastPos.Count)];
                            if (!exploitablePos.Find(item => item.Key.Equals(tmpPos)).Equals(default(KeyValuePair<Vector3, bool>)))
                            {
                                tmpPos = exploitablePos.Find(item => item.Key.Equals(tmpPos)).Key;
                                found = true;
                            }
                        }
                    }
                    
                    result = new KeyValuePair<Vector2, Vector3> (Vector2.right, tmpPos);
                }
                else
                {
                    if(northPos.Count != 0)
                    {
                        while (!found)
                        {
                            tmpPos = northPos[Random.Range(0, northPos.Count)];
                            if (!exploitablePos.Find(item => item.Key.Equals(tmpPos)).Equals(default(KeyValuePair<Vector3, bool>)))
                            {
                                tmpPos = exploitablePos.Find(item => item.Key.Equals(tmpPos)).Key;
                                found = true;
                            }
                        }
                    }
                    
                    result = new KeyValuePair<Vector2, Vector3>(Vector2.up, tmpPos);
                }
            }
            else
            {
                if(southPos.Count <= northPos.Count)
                {
                    if(southPos.Count != 0)
                    {
                        while (!found)
                        {
                            tmpPos = southPos[Random.Range(0, southPos.Count)];
                            if (!exploitablePos.Find(item => item.Key.Equals(tmpPos)).Equals(default(KeyValuePair<Vector3, bool>)))
                            {
                                tmpPos = exploitablePos.Find(item => item.Key.Equals(tmpPos)).Key;
                                found = true;
                            }
                        }
                    }
                    
                    result = new KeyValuePair<Vector2, Vector3>(Vector2.down, tmpPos);
                }
                else
                {
                    if(northPos.Count != 0)
                    {
                        while (!found)
                        {
                            tmpPos = northPos[Random.Range(0, northPos.Count)];
                            if (!exploitablePos.Find(item => item.Key.Equals(tmpPos)).Equals(default(KeyValuePair<Vector3, bool>)))
                            {
                                tmpPos = exploitablePos.Find(item => item.Key.Equals(tmpPos)).Key;
                                found = true;
                            }
                        }
                    }
                    
                    result = new KeyValuePair<Vector2, Vector3>(Vector2.up, tmpPos);
                }
            }
        }
        else
        {
            if (westPos.Count <= southPos.Count)
            {
                if (westPos.Count <= northPos.Count)
                {
                    if(westPos.Count != 0)
                    {
                        while (!found)
                        {
                            tmpPos = westPos[Random.Range(0, westPos.Count)];
                            if (!exploitablePos.Find(item => item.Key.Equals(tmpPos)).Equals(default(KeyValuePair<Vector3, bool>)))
                            {
                                tmpPos = exploitablePos.Find(item => item.Key.Equals(tmpPos)).Key;
                                found = true;
                            }
                        }
                    }
                    
                    result = new KeyValuePair<Vector2, Vector3>(Vector2.left, tmpPos);
                }
                else
                {
                    if(northPos.Count != 0)
                    {
                        while (!found)
                        {
                            tmpPos = northPos[Random.Range(0, northPos.Count)];
                            if (!exploitablePos.Find(item => item.Key.Equals(tmpPos)).Equals(default(KeyValuePair<Vector3, bool>)))
                            {
                                tmpPos = exploitablePos.Find(item => item.Key.Equals(tmpPos)).Key;
                                found = true;
                            }
                        }
                    }
                    
                    result = new KeyValuePair<Vector2, Vector3>(Vector2.up, tmpPos);
                }
            }
            else
            {
                if (southPos.Count <= northPos.Count)
                {
                    if(southPos.Count != 0)
                    {
                        while (!found)
                        {
                            tmpPos = southPos[Random.Range(0, southPos.Count)];
                            if (!exploitablePos.Find(item => item.Key.Equals(tmpPos)).Equals(default(KeyValuePair<Vector3, bool>)))
                            {
                                tmpPos = exploitablePos.Find(item => item.Key.Equals(tmpPos)).Key;
                                found = true;
                            }
                        }
                    }
                    
                    result = new KeyValuePair<Vector2, Vector3>(Vector2.down, tmpPos);
                }
                else
                {
                    if(northPos.Count != 0)
                    {
                        while (!found)
                        {
                            tmpPos = northPos[Random.Range(0, northPos.Count)];
                            if (!exploitablePos.Find(item => item.Key.Equals(tmpPos)).Equals(default(KeyValuePair<Vector3, bool>)))
                            {
                                tmpPos = exploitablePos.Find(item => item.Key.Equals(tmpPos)).Key;
                                found = true;
                            }
                        }
                    }
                    
                    result = new KeyValuePair<Vector2, Vector3>(Vector2.up, tmpPos);
                }
            }
        }

        return result;
    }

    //controllo se la posizione è occupata o meno da un tile
    private bool IsPosFree(Vector3 pos)
    {
        bool ok = true;

        foreach(Vector3 p in usedPositions)
        {
            if(Vector3.Distance(pos, p) < stepLenght)
            {
                ok = false;
            }
        }
        return ok;
    }

    //ottengo una posizione valida randomica in base alla categoria scelta (0 = sia stanze che corridoi, 1 = solo corridoi, 2 = solo stanze)
    private Vector3 GetRandomValidPos(int category)
    {
        if(exploitablePos.Count != 0)
        {
            if (category == 0)
            {
                return (exploitablePos[Random.Range(0, exploitablePos.Count)]).Key;
            }
            else if(category == 1)
            {
                List<KeyValuePair<Vector3, bool>> hallwayValidPos = exploitablePos.FindAll(item => item.Value.Equals(false));
                if(hallwayValidPos.Count != 0)
                {
                    return (hallwayValidPos[Random.Range(0, hallwayValidPos.Count)]).Key;
                }
                else
                {
                    return Vector3.one;
                }
            }
            else if(category == 2)
            {
                List<KeyValuePair<Vector3, bool>> roomValidPos = exploitablePos.FindAll(item => item.Value.Equals(true));
                if(roomValidPos.Count != 0)
                {
                    return (roomValidPos[Random.Range(0, roomValidPos.Count)]).Key;
                }
                else
                {
                    return Vector3.one;
                }
            }
            else
            {
                return Vector3.one;
            }
        }
        else
        {
            return Vector3.one;
        }
    }

    //fornisco una direzione random
    private Vector2 GetRandomDir()
    {
        Vector2 direction = Vector2.one;

        switch (Random.Range(0, 4))
        {
            case 0:
                direction = Vector2.up;
                break;
            case 1:
                direction = Vector2.down;
                break;
            case 2:
                direction = Vector2.left;
                break;
            case 3:
                direction = Vector2.right;
                break;
            default:
                break;
        }

        return direction;
    }
	
	// Update is called once per frame
	//void Update () {
		
	//}
}
